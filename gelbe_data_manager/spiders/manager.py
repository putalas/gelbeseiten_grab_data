# -*- coding: utf-8 -*-

import scrapy
from base64 import b64decode
from ..items import IdManagerItem
import csv

#scrapy crawl data -o data.csv -t csv

class GelbeSpider(scrapy.Spider):
    name = "data"

    def start_requests(self):
        id_list = []
        f1 = csv.reader(open('id_list.csv', 'rb'))
        for row in f1:
            id_list.append(row[0])

        for ids in id_list[:50]:
            url = 'http://adresse.gelbeseiten.de/' + str(ids)
            yield scrapy.Request(url=url, callback=self.parse)

    '''
    def write_to_file(self, txt):
        filename = 'data.csv'
        with open(filename, 'ab') as csvfile:
            writer = csv.writer(csvfile, delimiter='|',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(txt)
    '''
    def prep_info(self, resp, i):
        elem = resp.extract()
        if elem:
            if len(elem) > i:
                return elem[i]
        return ''

    def parse(self, response):
        url_sp = response.url.split('/')
        link_id = url_sp[-1]

        name = self.prep_info(response.xpath('//span[@itemprop="name"]//text()'), 0).encode('utf-8')
        if "|" in name:
            name = name.replace('|', '')
        street = self.prep_info(response.xpath('//span[@itemprop="streetAddress"]//text()'), 0).encode('utf-8')
        city = self.prep_info(response.xpath('//span[@itemprop="addressLocality"]//text()'), 0).encode('utf-8')
        postal = self.prep_info(response.xpath('//span[@itemprop="postalCode"]//text()'), 0).encode('utf-8')
        branche = self.prep_info(response.xpath("""//div[@class="branchen m08_branchen "]/
                                                     span//text()"""), 0).encode('utf-8')

        email = self.prep_info(response.xpath("""//li[@class="email "]/a[@class="link email_native_app"]/
                                                   span[@class="text"]/text()"""), 0).encode('utf-8')
        url_web = self.prep_info(response.xpath("""//ul[@class="profile"]//li[contains(@class, "website")]//
                                                     span[@class="text"]/text()"""), 0).encode('utf-8')

        # ===================================================
        phone1_nummer_attr = self.prep_info(response.xpath("""//div[@class="kommunikation m08_kommunikation"]/
                                     ul[@class="profile"]/li[@class="phone"]/a/
                                     span[@class="teilnehmertelefon"]/span/
                                     span[@class="nummer"]/text()"""), 0).encode('utf-8')

        phone1 = self.prep_info(response.xpath("""//div[@class="kommunikation m08_kommunikation"]/
                                     ul[@class="profile"]/li[@class="phone"]/a/
                                     span[@class="teilnehmertelefon"]/span/
                                     span/@data-telsuffix"""), 0).encode('utf-8')
        if phone1:
            phone1 = b64decode(phone1)
            if phone1_nummer_attr:
                phone1 = phone1_nummer_attr + phone1
        #===================================================
        phone2_nummer_attr = self.prep_info(response.xpath("""//span[@class="text nummer_ganz"]/
                                                    span[@class="nummer"]/text()"""), 0)
        phone2 = self.prep_info(response.xpath("""//span[@class="text nummer_ganz"]/
                                                    span/@data-telsuffix"""), 2).encode('utf-8')
        if phone2:
            phone2 = b64decode(phone2)
            if phone2_nummer_attr:
                phone2 = phone2_nummer_attr + phone2
        # ===================================================
        fax_nummer_attr = self.prep_info(response.xpath("""//span[@class="text nummer_ganz nummer_ganz_fax"]/
                                                             span[@class="nummer"]/text()"""), 0).encode('utf-8')
        fax = self.prep_info(response.xpath("""//span[@class="text nummer_ganz nummer_ganz_fax"]/
                                                 span/@data-telsuffix"""), 0).encode('utf-8')
        if fax:
            fax = b64decode(fax)
            if fax_nummer_attr:
                fax = fax_nummer_attr + fax
        # ===================================================

        #info = (name, street, postal, city, branche, phone1, email, url_web, phone2, fax, link_id)
        #self.write_to_file(info)

        item = IdManagerItem()
        item["name"] = name
        item["street"] = street
        item["city"] = city
        item["postal"] = postal
        item["branche"] = branche
        item["email"] = email
        item["url_web"] = url_web
        item["phone1"] = phone1
        item["phone2"] = phone2
        item["fax"] = fax
        item["link_id"] = link_id

        yield item
