# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class IdManagerItem(scrapy.Item):
    name = scrapy.Field()
    street = scrapy.Field()
    city = scrapy.Field()
    postal = scrapy.Field()
    branche = scrapy.Field()
    email = scrapy.Field()
    url_web = scrapy.Field()
    phone1 = scrapy.Field()
    phone2 = scrapy.Field()
    fax = scrapy.Field()
    link_id = scrapy.Field()
